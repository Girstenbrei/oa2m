# FROM python:3.8-slim
FROM golang:1.14-alpine as builder

WORKDIR /usr/app

COPY go.* /usr/app
RUN go mod download

COPY . /usr/app
CMD CGO_ENABLED=0 GOOS=windows go build -v -a -ldflags '-extldflags "-static"' -o ./oaAlerting main.go


# FROM scratch
# CMD [ "/oaAlerting", "--help" ]
# COPY --from=builder /usr/app/oaAlerting /
