/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"oaMonitoring/pkg/importer"
	"os"

	"github.com/google/go-jsonnet"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile       string
	loglevel      string
	logformat     string
	grafonnetPath string
	specFile      string
	inputPath     string
	outputPath    string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "oaMonitoring [spec-file]",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: run,
	Args: func(cmd *cobra.Command, args []string) (err error) {
		// Check number of arguments
		err = cobra.ExactArgs(1)(cmd, args)
		if err != nil {
			return
		}

		// Ensure file exists
		_, err = os.Stat(args[0])
		if err != nil {
			return
		}

		specFile = args[0]

		return
	},
}

func run(cmd *cobra.Command, args []string) {

	spec, err := importer.GatherSpecsFromFile(specFile)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not import openapi spec.")
	}

	// Instantiate jsonnet and add grafonnet to search path
	vm := jsonnet.MakeVM()
	vm.Importer(&jsonnet.FileImporter{
		JPaths: []string{grafonnetPath},
	})

	// Pars vars to jsonnet vm
	externalVars, err := json.MarshalIndent(spec.Endpoints, "", "    ")
	if err != nil {
		log.Fatal().Err(err).Msg("Could not parse variable to json for jsonnet.")
	}
	vm.ExtCode("endpoints", string(externalVars))

	// Load the source jsonnet file
	example, err := ioutil.ReadFile(inputPath)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not load jsonnet file.")
	}

	// Run the jsonnet
	result, err := vm.EvaluateSnippet(inputPath, string(example))
	if err != nil {
		log.Fatal().Err(err).Msg("Could not parse jsonnet file with err.")
	}

	// Write Output
	err = ioutil.WriteFile(outputPath, []byte(result), os.ModePerm)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not write output file.")
	}

	log.Info().Str("output", outputPath).Msg("Generated output successfully.")
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	cobra.OnInitialize(initLogger)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.oaMonitoring.yaml)")

	rootCmd.PersistentFlags().StringVarP(&loglevel, "loglevel", "l", "info", `Set the logging level ("debug"|"info"|"warn"|"error"|"fatal")`)
	rootCmd.PersistentFlags().StringVar(&logformat, "logformat", "human", `Set the logging format ("human"|"json")`)
	rootCmd.PersistentFlags().StringVarP(&grafonnetPath, "grafonnet", "g", "./grafonnet-lib", "The path to the grafonnet library.")
	rootCmd.PersistentFlags().StringVarP(&outputPath, "output", "o", "", "The path to put the generated file to.")
	rootCmd.PersistentFlags().StringVarP(&inputPath, "input", "i", "", "The path where the jsonnet dashboard source comes from.")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".oaMonitoring" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".oaMonitoring")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func initLogger() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(parseLogLevel(loglevel))

	if logformat == "human" {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	return
}

func parseLogLevel(logLevel string) zerolog.Level {
	switch logLevel {
	case "debug":
		return zerolog.DebugLevel
	case "info":
		return zerolog.InfoLevel
	case "warn":
		return zerolog.WarnLevel
	case "error":
		return zerolog.ErrorLevel
	case "panic":
		return zerolog.PanicLevel
	case "fatal":
		return zerolog.FatalLevel
	default:
		fmt.Printf("Could not parse loglevel %s\n", logLevel)
		os.Exit(1)
		return zerolog.PanicLevel
	}
}
