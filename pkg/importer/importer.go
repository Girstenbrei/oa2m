package importer

import (
	"encoding/json"
	"io/ioutil"

	"github.com/ghodss/yaml"
	"github.com/jinzhu/copier"
	"github.com/rs/zerolog/log"
)

// Panel is the specification of what jsonnet to use
type Panel struct {
	Source string `json:"source"`
}

// MonitoringSpec is a single specification for a monitoring endpoint
type MonitoringSpec struct {
	Name     string                 `json:"name"`
	Type     string                 `json:"type"`
	Selector string                 `json:"selector"`
	Panel    Panel                  `json:"panel"`
	Source   map[string]interface{} `json:"source"`
}

type MonitoringSettings struct {
	File     string                    `json:"file"`
	Defaults map[string]MonitoringSpec `json:"defaults"`
}

// The interesting parts of an OpenAPI-file
type OpenApi struct {
	XMonitoringSettings MonitoringSettings `json:"x-monitoring"`
	Endpoints           []MonitoringSpec   `json:"-"`
}

func extractEndpointSpecs(value map[string]interface{}, spec *MonitoringSpec) {
	rawSettings, ok := value["x-monitoring"]
	if !ok {
		log.Debug().Msg("Skipping as no settings in endpoint.")
		return
	}

	j, err := json.Marshal(rawSettings)
	if err != nil {
		log.Error().Err(err).Msg("Could not marshall.")
		return
	}

	err = json.Unmarshal(j, spec)
	if err != nil {
		log.Error().Err(err).Msg("Could not unmarshall.")
		return
	}

	return
}

func getEndpointValues(spec *OpenApi, content []byte) (err error) {

	rawContent := map[string]interface{}{}
	err = yaml.Unmarshal(content, &rawContent)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not convert source information to json.")
		return
	}

	for typ, deflt := range spec.XMonitoringSettings.Defaults {

		if x, found := rawContent[typ]; found {
			paths, ok := x.(map[string]interface{})
			if !ok {
				log.Fatal().Err(err).Msg("Could not convert source to map.")
			}

			// For every path, get name and value
			for name, rawPath := range paths {
				// Create new endpoint Spec and set defaults
				endpointSpec := new(MonitoringSpec)
				copier.Copy(&endpointSpec, &deflt)
				endpointSpec.Type = typ
				endpointSpec.Name = name
				log.Info().Str("name", endpointSpec.Name).Str("type", endpointSpec.Type).Msg("Processing endpoint.")

				// Get actual values
				path, ok := rawPath.(map[string]interface{})
				endpointSpec.Source = path
				if !ok {
					log.Debug().Str("name", name).Msg("Using defaults.")
					continue
				}
				extractEndpointSpecs(path, endpointSpec)

				// Append to endpoints
				spec.Endpoints = append(spec.Endpoints, *endpointSpec)
			}
		}
	}

	return
}

// GatherSpecsFromFile extracts the source information from an Openapi Spec file.
func GatherSpecsFromFile(path string) (specs OpenApi, err error) {

	// Parse
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}

	// spec := OpenApi{}
	err = yaml.Unmarshal(content, &specs)
	if err != nil {
		return
	}

	// Get values for endpoints
	err = getEndpointValues(&specs, content)
	if err != nil {
		return
	}

	return
}
