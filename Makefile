build:
	go build -v -o ./build/oaAlerting.exe ./main.go

run: build
 	# echo '{"foo": 0}' | jq .
	./build/oaAlerting.exe tests/petstore.yaml

.PHONY: build run

ifndef VERBOSE
.SILENT:
endif