// Grafana
local grafana = import 'grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local template = grafana.template;
local singlestat = grafana.singlestat;
local graphPanel = grafana.graphPanel;
local prometheus = grafana.prometheus;
local text = grafana.text;

// The endpoints from the converter
local endpoints = std.extVar('endpoints');

// local rest = graphPanel.new(
//     title='Rest Graph',
//     datasource='Prometheus',
//     linewidth=2,
//     format='Bps',
//     aliasColors={
//       Rx: 'light-green',
//       Tx: 'light-red',
//     },
//   ).addTarget(
//     prometheus.target(
//       'rate(node_network_receive_bytes_total{instance="$instance",device="eth0"}[1m])',
//       legendFormat='Rx',
//     )
//   ).addTarget(
//     prometheus.target(
//       'irate(node_network_transmit_bytes_total{instance="$instance",device="eth0"}[1m]) * (-1)',
//       legendFormat='Tx',
//     )
// );

local new_endpoint(spec) = 
    local safeName = std.strReplace(spec.name, "{", "<");
    local saferName = std.strReplace(safeName, "}", ">");
    local procSelector = std.strReplace(spec.selector, "{{name}}", '"' + saferName + '"');

    graphPanel.new(
    title=spec.type + ': ' + saferName,
    datasource='Prometheus',
    linewidth=2,
    format='Bps',
    aliasColors={
      Rate: 'light-green',
      Errors: 'light-red',
    },
  ).addTarget(
    prometheus.target(
      'rate(http_server_requests{' + procSelector + ',outcome="success"}[1m])',
      legendFormat='Rate',
    )
  ).addTarget(
    prometheus.target(
      'rate(http_server_requests{' + procSelector + ',outcome="error"}[1m])',
      legendFormat='Errors',
    )
);

local debug = text.new(
  title="Debug",
  content=std.toString(endpoints)
);

dashboard.new(
    "OpenApi Service",
    tags=['prometheus'],
    schemaVersion=18,
    editable=true,
    time_from='now-1h',
    refresh='10s',
)

.addPanels(
    [new_endpoint(spec) {
        gridPos: {
            x: 1,
            y: 0,
            w: 24,
            h: 6,
        }
    } for spec in endpoints] ,
)

.addPanels(
  [debug{
        gridPos: {
            x: 2,
            y: 0,
            w: 24,
            h: 6,
        }
    }],
)
